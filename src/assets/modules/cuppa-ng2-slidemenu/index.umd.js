/**
 * cuppa-ng2-slidemenu - Angular 2 slide in menu for web and mobile. Also referred to as slide navigation menu.
 * @version v2.0.0
 * @author undefined
 * @link 
 * @license MIT
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@angular/core"), require("@angular/animations"), require("@angular/common"), require("@angular/platform-browser"), require("@angular/platform-browser/animations"));
	else if(typeof define === 'function' && define.amd)
		define(["@angular/core", "@angular/animations", "@angular/common", "@angular/platform-browser", "@angular/platform-browser/animations"], factory);
	else if(typeof exports === 'object')
		exports["ticktock"] = factory(require("@angular/core"), require("@angular/animations"), require("@angular/common"), require("@angular/platform-browser"), require("@angular/platform-browser/animations"));
	else
		root["ticktock"] = factory(root["ng"]["core"], root["ng"]["animations"], root["ng"]["common"], root["ng"]["platformBrowser"], root["ng"]["platformBrowserAnimations"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_10__, __WEBPACK_EXTERNAL_MODULE_11__, __WEBPACK_EXTERNAL_MODULE_12__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(3);
var platform_browser_1 = __webpack_require__(11);
var common_1 = __webpack_require__(10);
var animations_1 = __webpack_require__(12);
var animations_2 = __webpack_require__(9);
var clickOutside_1 = __webpack_require__(1);
var SlideMenu = /** @class */ (function () {
    function SlideMenu(_elementRef, sanitizer) {
        this._elementRef = _elementRef;
        this.sanitizer = sanitizer;
        this.open = new core_1.EventEmitter();
        this.close = new core_1.EventEmitter();
        this.itemSelect = new core_1.EventEmitter();
        this.defaultConfig = {
            "animation": "collapse",
            "offset": {
                "top": 55
            },
            closeOnCLick: false
        };
    }
    SlideMenu.prototype.ngOnInit = function () {
        this.menuState = false;
        this.config = Object.assign(this.defaultConfig, this.config);
        this.addOverlayElement();
    };
    SlideMenu.prototype.ngAfterViewInit = function () {
    };
    SlideMenu.prototype.menuToggle = function () {
        this.menuState = !this.menuState;
        this.toggleOverlay();
        if (this.menuState) {
            this.open.emit();
        }
        else {
            this.close.emit();
        }
    };
    SlideMenu.prototype.closeMenu = function () {
        this.menuState = false;
        this.overlayElem.style['opacity'] = 0;
    };
    SlideMenu.prototype.onItemClick = function (item) {
        if (this.currentItem) {
            this.currentItem.active = this.currentItem.active ? false : true;
        }
        this.currentItem = item;
        item.active = true;
        if (item.subItems) {
            return false;
        }
        else {
            delete item["expand"];
            var obj = Object.assign(item);
            this.itemSelect.emit(obj);
            if (this.config.closeOnCLick) {
                this.closeMenu();
            }
        }
    };
    SlideMenu.prototype.toggleSubMenu = function (item) {
        if (item.expand) {
            item.expand = item.expand == 'hide' ? 'show' : 'hide';
        }
        else {
            item.expand = 'show';
        }
    };
    SlideMenu.prototype.addOverlayElement = function () {
        this.overlayElem = document.createElement('div');
        this.overlayElem.classList.add('cuppa-menu-overlay');
        this.overlayElem.style['position'] = 'fixed';
        this.overlayElem.style['background'] = 'rgba(0, 0, 0, 0.7)';
        this.overlayElem.style['top'] = this.config.offset.top + 'px';
        this.overlayElem.style['left'] = 0;
        this.overlayElem.style['right'] = 0;
        this.overlayElem.style['bottom'] = 0;
        this.overlayElem.style['opacity'] = 0;
        this.overlayElem.style['pointer-events'] = 'none';
        this.overlayElem.style['transition'] = 'all .2s linear';
        document.getElementsByTagName('body')[0].appendChild(this.overlayElem);
    };
    SlideMenu.prototype.toggleOverlay = function () {
        if (this.overlayElem.style['opacity'] == 0) {
            this.overlayElem.style['opacity'] = 1;
        }
        else if (this.overlayElem.style['opacity'] == 1) {
            this.overlayElem.style['opacity'] = 0;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], SlideMenu.prototype, "menulist", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], SlideMenu.prototype, "config", void 0);
    __decorate([
        core_1.Output('open'),
        __metadata("design:type", core_1.EventEmitter)
    ], SlideMenu.prototype, "open", void 0);
    __decorate([
        core_1.Output('close'),
        __metadata("design:type", core_1.EventEmitter)
    ], SlideMenu.prototype, "close", void 0);
    __decorate([
        core_1.Output('onItemSelect'),
        __metadata("design:type", core_1.EventEmitter)
    ], SlideMenu.prototype, "itemSelect", void 0);
    SlideMenu = __decorate([
        core_1.Component({
            selector: 'cuppa-slidemenu',
            template: __webpack_require__(6),
            styles: [__webpack_require__(7), __webpack_require__(8)],
            animations: [
                animations_2.trigger('toggleMenu', [
                    animations_2.state('show', animations_2.style({ height: '*' })),
                    animations_2.state('hide', animations_2.style({ height: 0 })),
                    animations_2.transition('void => *', [
                        animations_2.style({ height: 0, overflow: 'hidden' })
                    ]),
                    animations_2.transition('* => hide', [
                        animations_2.style({ height: '*' }),
                        animations_2.animate(250, animations_2.style({ height: 0 }))
                    ]),
                    animations_2.transition('hide => show', [
                        animations_2.style({ height: 0 }),
                        animations_2.animate(250, animations_2.style({ height: '*' }))
                    ])
                ]),
                animations_2.trigger('toggleArrow', [
                    animations_2.state('right', animations_2.style({ transform: 'rotate(0)' })),
                    animations_2.state('down', animations_2.style({ transform: 'rotate(90deg)' })),
                    animations_2.transition('right <=> down', animations_2.animate('100ms ease-in'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, platform_browser_1.DomSanitizer])
    ], SlideMenu);
    return SlideMenu;
}());
exports.SlideMenu = SlideMenu;
var SlideMenuModule = /** @class */ (function () {
    function SlideMenuModule() {
    }
    SlideMenuModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, animations_1.BrowserAnimationsModule],
            declarations: [SlideMenu, clickOutside_1.ClickOutsideDirective],
            exports: [SlideMenu, clickOutside_1.ClickOutsideDirective, animations_1.BrowserAnimationsModule]
        })
    ], SlideMenuModule);
    return SlideMenuModule;
}());
exports.SlideMenuModule = SlideMenuModule;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(3);
var ClickOutsideDirective = /** @class */ (function () {
    function ClickOutsideDirective(_elementRef) {
        this._elementRef = _elementRef;
        this.clickOutside = new core_1.EventEmitter();
    }
    ClickOutsideDirective.prototype.onClick = function (event, targetElement) {
        if (!targetElement) {
            return;
        }
        var clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.clickOutside.emit(event);
        }
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ClickOutsideDirective.prototype, "clickOutside", void 0);
    __decorate([
        core_1.HostListener('document:click', ['$event', '$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [MouseEvent, HTMLElement]),
        __metadata("design:returntype", void 0)
    ], ClickOutsideDirective.prototype, "onClick", null);
    ClickOutsideDirective = __decorate([
        core_1.Directive({
            selector: '[clickOutside]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], ClickOutsideDirective);
    return ClickOutsideDirective;
}());
exports.ClickOutsideDirective = ClickOutsideDirective;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(undefined);
// imports


// module
exports.push([module.i, "/*!\r\n * Hamburgers\r\n * @description Tasty CSS-animated hamburgers\r\n * @author Jonathan Suh @jonsuh\r\n * @site https://jonsuh.com/hamburgers\r\n * @link https://github.com/jonsuh/hamburgers\r\n */\n.hamburger {\n  padding: 15px 15px;\n  display: inline-block;\n  cursor: pointer;\n  transition-property: opacity, filter;\n  transition-duration: 0.15s;\n  transition-timing-function: linear;\n  font: inherit;\n  color: inherit;\n  text-transform: none;\n  background-color: transparent;\n  border: 0;\n  margin: 0;\n  overflow: visible; }\n  .hamburger:hover {\n    opacity: 0.7; }\n  .hamburger:focus {\n    outline: none; }\n\n.hamburger-box {\n  width: 40px;\n  height: 24px;\n  display: block;\n  position: relative; }\n\n.hamburger-inner {\n  display: block;\n  top: 50%;\n  margin-top: -2px; }\n  .hamburger-inner, .hamburger-inner::before, .hamburger-inner::after {\n    width: 40px;\n    height: 4px;\n    background-color: #333;\n    border-radius: 4px;\n    position: absolute;\n    transition-property: transform;\n    transition-duration: 0.15s;\n    transition-timing-function: ease; }\n  .hamburger-inner::before, .hamburger-inner::after {\n    content: \"\";\n    display: block; }\n  .hamburger-inner::before {\n    top: -10px; }\n  .hamburger-inner::after {\n    bottom: -10px; }\n\n/*\n   * 3DX\n   */\n.hamburger--3dx .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dx .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dx .hamburger-inner::before, .hamburger--3dx .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dx.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateY(180deg); }\n  .hamburger--3dx.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dx.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * 3DX Reverse\n   */\n.hamburger--3dx-r .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dx-r .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dx-r .hamburger-inner::before, .hamburger--3dx-r .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dx-r.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateY(-180deg); }\n  .hamburger--3dx-r.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dx-r.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * 3DY\n   */\n.hamburger--3dy .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dy .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dy .hamburger-inner::before, .hamburger--3dy .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dy.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateX(-180deg); }\n  .hamburger--3dy.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dy.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * 3DY Reverse\n   */\n.hamburger--3dy-r .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dy-r .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dy-r .hamburger-inner::before, .hamburger--3dy-r .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dy-r.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateX(180deg); }\n  .hamburger--3dy-r.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dy-r.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * 3DXY\n   */\n.hamburger--3dxy .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dxy .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dxy .hamburger-inner::before, .hamburger--3dxy .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dxy.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateX(180deg) rotateY(180deg); }\n  .hamburger--3dxy.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dxy.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * 3DXY Reverse\n   */\n.hamburger--3dxy-r .hamburger-box {\n  perspective: 80px; }\n\n.hamburger--3dxy-r .hamburger-inner {\n  transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n  .hamburger--3dxy-r .hamburger-inner::before, .hamburger--3dxy-r .hamburger-inner::after {\n    transition: transform 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1); }\n\n.hamburger--3dxy-r.is-active .hamburger-inner {\n  background-color: transparent;\n  transform: rotateX(180deg) rotateY(180deg) rotateZ(-180deg); }\n  .hamburger--3dxy-r.is-active .hamburger-inner::before {\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--3dxy-r.is-active .hamburger-inner::after {\n    transform: translate3d(0, -10px, 0) rotate(-45deg); }\n\n/*\n   * Arrow\n   */\n.hamburger--arrow.is-active .hamburger-inner::before {\n  transform: translate3d(-8px, 0, 0) rotate(-45deg) scale(0.7, 1); }\n\n.hamburger--arrow.is-active .hamburger-inner::after {\n  transform: translate3d(-8px, 0, 0) rotate(45deg) scale(0.7, 1); }\n\n/*\n   * Arrow Right\n   */\n.hamburger--arrow-r.is-active .hamburger-inner::before {\n  transform: translate3d(8px, 0, 0) rotate(45deg) scale(0.7, 1); }\n\n.hamburger--arrow-r.is-active .hamburger-inner::after {\n  transform: translate3d(8px, 0, 0) rotate(-45deg) scale(0.7, 1); }\n\n/*\n   * Arrow Alt\n   */\n.hamburger--arrowalt .hamburger-inner::before {\n  transition: top 0.1s 0.1s ease, transform 0.1s cubic-bezier(0.165, 0.84, 0.44, 1); }\n\n.hamburger--arrowalt .hamburger-inner::after {\n  transition: bottom 0.1s 0.1s ease, transform 0.1s cubic-bezier(0.165, 0.84, 0.44, 1); }\n\n.hamburger--arrowalt.is-active .hamburger-inner::before {\n  top: 0;\n  transform: translate3d(-8px, -10px, 0) rotate(-45deg) scale(0.7, 1);\n  transition: top 0.1s ease, transform 0.1s 0.1s cubic-bezier(0.895, 0.03, 0.685, 0.22); }\n\n.hamburger--arrowalt.is-active .hamburger-inner::after {\n  bottom: 0;\n  transform: translate3d(-8px, 10px, 0) rotate(45deg) scale(0.7, 1);\n  transition: bottom 0.1s ease, transform 0.1s 0.1s cubic-bezier(0.895, 0.03, 0.685, 0.22); }\n\n/*\n   * Arrow Alt Right\n   */\n.hamburger--arrowalt-r .hamburger-inner::before {\n  transition: top 0.1s 0.1s ease, transform 0.1s cubic-bezier(0.165, 0.84, 0.44, 1); }\n\n.hamburger--arrowalt-r .hamburger-inner::after {\n  transition: bottom 0.1s 0.1s ease, transform 0.1s cubic-bezier(0.165, 0.84, 0.44, 1); }\n\n.hamburger--arrowalt-r.is-active .hamburger-inner::before {\n  top: 0;\n  transform: translate3d(8px, -10px, 0) rotate(45deg) scale(0.7, 1);\n  transition: top 0.1s ease, transform 0.1s 0.1s cubic-bezier(0.895, 0.03, 0.685, 0.22); }\n\n.hamburger--arrowalt-r.is-active .hamburger-inner::after {\n  bottom: 0;\n  transform: translate3d(8px, 10px, 0) rotate(-45deg) scale(0.7, 1);\n  transition: bottom 0.1s ease, transform 0.1s 0.1s cubic-bezier(0.895, 0.03, 0.685, 0.22); }\n\n/*\n * Arrow Turn\n */\n.hamburger--arrowturn.is-active .hamburger-inner {\n  transform: rotate(-180deg); }\n  .hamburger--arrowturn.is-active .hamburger-inner::before {\n    transform: translate3d(8px, 0, 0) rotate(45deg) scale(0.7, 1); }\n  .hamburger--arrowturn.is-active .hamburger-inner::after {\n    transform: translate3d(8px, 0, 0) rotate(-45deg) scale(0.7, 1); }\n\n/*\n * Arrow Turn Right\n */\n.hamburger--arrowturn-r.is-active .hamburger-inner {\n  transform: rotate(-180deg); }\n  .hamburger--arrowturn-r.is-active .hamburger-inner::before {\n    transform: translate3d(-8px, 0, 0) rotate(-45deg) scale(0.7, 1); }\n  .hamburger--arrowturn-r.is-active .hamburger-inner::after {\n    transform: translate3d(-8px, 0, 0) rotate(45deg) scale(0.7, 1); }\n\n/*\n   * Boring\n   */\n.hamburger--boring .hamburger-inner, .hamburger--boring .hamburger-inner::before, .hamburger--boring .hamburger-inner::after {\n  transition-property: none; }\n\n.hamburger--boring.is-active .hamburger-inner {\n  transform: rotate(45deg); }\n  .hamburger--boring.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0; }\n  .hamburger--boring.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(-90deg); }\n\n/*\n   * Collapse\n   */\n.hamburger--collapse .hamburger-inner {\n  top: auto;\n  bottom: 0;\n  transition-duration: 0.13s;\n  transition-delay: 0.13s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--collapse .hamburger-inner::after {\n    top: -20px;\n    transition: top 0.2s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), opacity 0.1s linear; }\n  .hamburger--collapse .hamburger-inner::before {\n    transition: top 0.12s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), transform 0.13s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--collapse.is-active .hamburger-inner {\n  transform: translate3d(0, -10px, 0) rotate(-45deg);\n  transition-delay: 0.22s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--collapse.is-active .hamburger-inner::after {\n    top: 0;\n    opacity: 0;\n    transition: top 0.2s cubic-bezier(0.33333, 0, 0.66667, 0.33333), opacity 0.1s 0.22s linear; }\n  .hamburger--collapse.is-active .hamburger-inner::before {\n    top: 0;\n    transform: rotate(-90deg);\n    transition: top 0.1s 0.16s cubic-bezier(0.33333, 0, 0.66667, 0.33333), transform 0.13s 0.25s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Collapse Reverse\n   */\n.hamburger--collapse-r .hamburger-inner {\n  top: auto;\n  bottom: 0;\n  transition-duration: 0.13s;\n  transition-delay: 0.13s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--collapse-r .hamburger-inner::after {\n    top: -20px;\n    transition: top 0.2s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), opacity 0.1s linear; }\n  .hamburger--collapse-r .hamburger-inner::before {\n    transition: top 0.12s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), transform 0.13s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--collapse-r.is-active .hamburger-inner {\n  transform: translate3d(0, -10px, 0) rotate(45deg);\n  transition-delay: 0.22s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--collapse-r.is-active .hamburger-inner::after {\n    top: 0;\n    opacity: 0;\n    transition: top 0.2s cubic-bezier(0.33333, 0, 0.66667, 0.33333), opacity 0.1s 0.22s linear; }\n  .hamburger--collapse-r.is-active .hamburger-inner::before {\n    top: 0;\n    transform: rotate(90deg);\n    transition: top 0.1s 0.16s cubic-bezier(0.33333, 0, 0.66667, 0.33333), transform 0.13s 0.25s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Elastic\n   */\n.hamburger--elastic .hamburger-inner {\n  top: 2px;\n  transition-duration: 0.275s;\n  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55); }\n  .hamburger--elastic .hamburger-inner::before {\n    top: 10px;\n    transition: opacity 0.125s 0.275s ease; }\n  .hamburger--elastic .hamburger-inner::after {\n    top: 20px;\n    transition: transform 0.275s cubic-bezier(0.68, -0.55, 0.265, 1.55); }\n\n.hamburger--elastic.is-active .hamburger-inner {\n  transform: translate3d(0, 10px, 0) rotate(135deg);\n  transition-delay: 0.075s; }\n  .hamburger--elastic.is-active .hamburger-inner::before {\n    transition-delay: 0s;\n    opacity: 0; }\n  .hamburger--elastic.is-active .hamburger-inner::after {\n    transform: translate3d(0, -20px, 0) rotate(-270deg);\n    transition-delay: 0.075s; }\n\n/*\n   * Elastic Reverse\n   */\n.hamburger--elastic-r .hamburger-inner {\n  top: 2px;\n  transition-duration: 0.275s;\n  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55); }\n  .hamburger--elastic-r .hamburger-inner::before {\n    top: 10px;\n    transition: opacity 0.125s 0.275s ease; }\n  .hamburger--elastic-r .hamburger-inner::after {\n    top: 20px;\n    transition: transform 0.275s cubic-bezier(0.68, -0.55, 0.265, 1.55); }\n\n.hamburger--elastic-r.is-active .hamburger-inner {\n  transform: translate3d(0, 10px, 0) rotate(-135deg);\n  transition-delay: 0.075s; }\n  .hamburger--elastic-r.is-active .hamburger-inner::before {\n    transition-delay: 0s;\n    opacity: 0; }\n  .hamburger--elastic-r.is-active .hamburger-inner::after {\n    transform: translate3d(0, -20px, 0) rotate(270deg);\n    transition-delay: 0.075s; }\n\n/*\n   * Emphatic\n   */\n.hamburger--emphatic {\n  overflow: hidden; }\n  .hamburger--emphatic .hamburger-inner {\n    transition: background-color 0.125s 0.175s ease-in; }\n    .hamburger--emphatic .hamburger-inner::before {\n      left: 0;\n      transition: transform 0.125s cubic-bezier(0.6, 0.04, 0.98, 0.335), top 0.05s 0.125s linear, left 0.125s 0.175s ease-in; }\n    .hamburger--emphatic .hamburger-inner::after {\n      top: 10px;\n      right: 0;\n      transition: transform 0.125s cubic-bezier(0.6, 0.04, 0.98, 0.335), top 0.05s 0.125s linear, right 0.125s 0.175s ease-in; }\n  .hamburger--emphatic.is-active .hamburger-inner {\n    transition-delay: 0s;\n    transition-timing-function: ease-out;\n    background-color: transparent; }\n    .hamburger--emphatic.is-active .hamburger-inner::before {\n      left: -80px;\n      top: -80px;\n      transform: translate3d(80px, 80px, 0) rotate(45deg);\n      transition: left 0.125s ease-out, top 0.05s 0.125s linear, transform 0.125s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1); }\n    .hamburger--emphatic.is-active .hamburger-inner::after {\n      right: -80px;\n      top: -80px;\n      transform: translate3d(-80px, 80px, 0) rotate(-45deg);\n      transition: right 0.125s ease-out, top 0.05s 0.125s linear, transform 0.125s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1); }\n\n/*\n   * Emphatic Reverse\n   */\n.hamburger--emphatic-r {\n  overflow: hidden; }\n  .hamburger--emphatic-r .hamburger-inner {\n    transition: background-color 0.125s 0.175s ease-in; }\n    .hamburger--emphatic-r .hamburger-inner::before {\n      left: 0;\n      transition: transform 0.125s cubic-bezier(0.6, 0.04, 0.98, 0.335), top 0.05s 0.125s linear, left 0.125s 0.175s ease-in; }\n    .hamburger--emphatic-r .hamburger-inner::after {\n      top: 10px;\n      right: 0;\n      transition: transform 0.125s cubic-bezier(0.6, 0.04, 0.98, 0.335), top 0.05s 0.125s linear, right 0.125s 0.175s ease-in; }\n  .hamburger--emphatic-r.is-active .hamburger-inner {\n    transition-delay: 0s;\n    transition-timing-function: ease-out;\n    background-color: transparent; }\n    .hamburger--emphatic-r.is-active .hamburger-inner::before {\n      left: -80px;\n      top: 80px;\n      transform: translate3d(80px, -80px, 0) rotate(-45deg);\n      transition: left 0.125s ease-out, top 0.05s 0.125s linear, transform 0.125s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1); }\n    .hamburger--emphatic-r.is-active .hamburger-inner::after {\n      right: -80px;\n      top: 80px;\n      transform: translate3d(-80px, -80px, 0) rotate(45deg);\n      transition: right 0.125s ease-out, top 0.05s 0.125s linear, transform 0.125s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1); }\n\n/*\n   * Minus\n   */\n.hamburger--minus .hamburger-inner::before, .hamburger--minus .hamburger-inner::after {\n  transition: bottom 0.08s 0s ease-out, top 0.08s 0s ease-out, opacity 0s linear; }\n\n.hamburger--minus.is-active .hamburger-inner::before, .hamburger--minus.is-active .hamburger-inner::after {\n  opacity: 0;\n  transition: bottom 0.08s ease-out, top 0.08s ease-out, opacity 0s 0.08s linear; }\n\n.hamburger--minus.is-active .hamburger-inner::before {\n  top: 0; }\n\n.hamburger--minus.is-active .hamburger-inner::after {\n  bottom: 0; }\n\n/*\n   * Slider\n   */\n.hamburger--slider .hamburger-inner {\n  top: 2px; }\n  .hamburger--slider .hamburger-inner::before {\n    top: 10px;\n    transition-property: transform, opacity;\n    transition-timing-function: ease;\n    transition-duration: 0.15s; }\n  .hamburger--slider .hamburger-inner::after {\n    top: 20px; }\n\n.hamburger--slider.is-active .hamburger-inner {\n  transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--slider.is-active .hamburger-inner::before {\n    transform: rotate(-45deg) translate3d(-5.71429px, -6px, 0);\n    opacity: 0; }\n  .hamburger--slider.is-active .hamburger-inner::after {\n    transform: translate3d(0, -20px, 0) rotate(-90deg); }\n\n/*\n   * Slider Reverse\n   */\n.hamburger--slider-r .hamburger-inner {\n  top: 2px; }\n  .hamburger--slider-r .hamburger-inner::before {\n    top: 10px;\n    transition-property: transform, opacity;\n    transition-timing-function: ease;\n    transition-duration: 0.15s; }\n  .hamburger--slider-r .hamburger-inner::after {\n    top: 20px; }\n\n.hamburger--slider-r.is-active .hamburger-inner {\n  transform: translate3d(0, 10px, 0) rotate(-45deg); }\n  .hamburger--slider-r.is-active .hamburger-inner::before {\n    transform: rotate(45deg) translate3d(5.71429px, -6px, 0);\n    opacity: 0; }\n  .hamburger--slider-r.is-active .hamburger-inner::after {\n    transform: translate3d(0, -20px, 0) rotate(90deg); }\n\n/*\n   * Spin\n   */\n.hamburger--spin .hamburger-inner {\n  transition-duration: 0.22s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--spin .hamburger-inner::before {\n    transition: top 0.1s 0.25s ease-in, opacity 0.1s ease-in; }\n  .hamburger--spin .hamburger-inner::after {\n    transition: bottom 0.1s 0.25s ease-in, transform 0.22s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--spin.is-active .hamburger-inner {\n  transform: rotate(225deg);\n  transition-delay: 0.12s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--spin.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0;\n    transition: top 0.1s ease-out, opacity 0.1s 0.12s ease-out; }\n  .hamburger--spin.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(-90deg);\n    transition: bottom 0.1s ease-out, transform 0.22s 0.12s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Spin Reverse\n   */\n.hamburger--spin-r .hamburger-inner {\n  transition-duration: 0.22s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--spin-r .hamburger-inner::before {\n    transition: top 0.1s 0.25s ease-in, opacity 0.1s ease-in; }\n  .hamburger--spin-r .hamburger-inner::after {\n    transition: bottom 0.1s 0.25s ease-in, transform 0.22s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--spin-r.is-active .hamburger-inner {\n  transform: rotate(-225deg);\n  transition-delay: 0.12s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--spin-r.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0;\n    transition: top 0.1s ease-out, opacity 0.1s 0.12s ease-out; }\n  .hamburger--spin-r.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(90deg);\n    transition: bottom 0.1s ease-out, transform 0.22s 0.12s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Spring\n   */\n.hamburger--spring .hamburger-inner {\n  top: 2px;\n  transition: background-color 0s 0.13s linear; }\n  .hamburger--spring .hamburger-inner::before {\n    top: 10px;\n    transition: top 0.1s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), transform 0.13s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--spring .hamburger-inner::after {\n    top: 20px;\n    transition: top 0.2s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), transform 0.13s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--spring.is-active .hamburger-inner {\n  transition-delay: 0.22s;\n  background-color: transparent; }\n  .hamburger--spring.is-active .hamburger-inner::before {\n    top: 0;\n    transition: top 0.1s 0.15s cubic-bezier(0.33333, 0, 0.66667, 0.33333), transform 0.13s 0.22s cubic-bezier(0.215, 0.61, 0.355, 1);\n    transform: translate3d(0, 10px, 0) rotate(45deg); }\n  .hamburger--spring.is-active .hamburger-inner::after {\n    top: 0;\n    transition: top 0.2s cubic-bezier(0.33333, 0, 0.66667, 0.33333), transform 0.13s 0.22s cubic-bezier(0.215, 0.61, 0.355, 1);\n    transform: translate3d(0, 10px, 0) rotate(-45deg); }\n\n/*\n   * Spring Reverse\n   */\n.hamburger--spring-r .hamburger-inner {\n  top: auto;\n  bottom: 0;\n  transition-duration: 0.13s;\n  transition-delay: 0s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--spring-r .hamburger-inner::after {\n    top: -20px;\n    transition: top 0.2s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), opacity 0s linear; }\n  .hamburger--spring-r .hamburger-inner::before {\n    transition: top 0.1s 0.2s cubic-bezier(0.33333, 0.66667, 0.66667, 1), transform 0.13s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--spring-r.is-active .hamburger-inner {\n  transform: translate3d(0, -10px, 0) rotate(-45deg);\n  transition-delay: 0.22s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--spring-r.is-active .hamburger-inner::after {\n    top: 0;\n    opacity: 0;\n    transition: top 0.2s cubic-bezier(0.33333, 0, 0.66667, 0.33333), opacity 0s 0.22s linear; }\n  .hamburger--spring-r.is-active .hamburger-inner::before {\n    top: 0;\n    transform: rotate(90deg);\n    transition: top 0.1s 0.15s cubic-bezier(0.33333, 0, 0.66667, 0.33333), transform 0.13s 0.22s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Stand\n   */\n.hamburger--stand .hamburger-inner {\n  transition: transform 0.075s 0.15s cubic-bezier(0.55, 0.055, 0.675, 0.19), background-color 0s 0.075s linear; }\n  .hamburger--stand .hamburger-inner::before {\n    transition: top 0.075s 0.075s ease-in, transform 0.075s 0s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--stand .hamburger-inner::after {\n    transition: bottom 0.075s 0.075s ease-in, transform 0.075s 0s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--stand.is-active .hamburger-inner {\n  transform: rotate(90deg);\n  background-color: transparent;\n  transition: transform 0.075s 0s cubic-bezier(0.215, 0.61, 0.355, 1), background-color 0s 0.15s linear; }\n  .hamburger--stand.is-active .hamburger-inner::before {\n    top: 0;\n    transform: rotate(-45deg);\n    transition: top 0.075s 0.1s ease-out, transform 0.075s 0.15s cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--stand.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(45deg);\n    transition: bottom 0.075s 0.1s ease-out, transform 0.075s 0.15s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Stand Reverse\n   */\n.hamburger--stand-r .hamburger-inner {\n  transition: transform 0.075s 0.15s cubic-bezier(0.55, 0.055, 0.675, 0.19), background-color 0s 0.075s linear; }\n  .hamburger--stand-r .hamburger-inner::before {\n    transition: top 0.075s 0.075s ease-in, transform 0.075s 0s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--stand-r .hamburger-inner::after {\n    transition: bottom 0.075s 0.075s ease-in, transform 0.075s 0s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--stand-r.is-active .hamburger-inner {\n  transform: rotate(-90deg);\n  background-color: transparent;\n  transition: transform 0.075s 0s cubic-bezier(0.215, 0.61, 0.355, 1), background-color 0s 0.15s linear; }\n  .hamburger--stand-r.is-active .hamburger-inner::before {\n    top: 0;\n    transform: rotate(-45deg);\n    transition: top 0.075s 0.1s ease-out, transform 0.075s 0.15s cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--stand-r.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(45deg);\n    transition: bottom 0.075s 0.1s ease-out, transform 0.075s 0.15s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Squeeze\n   */\n.hamburger--squeeze .hamburger-inner {\n  transition-duration: 0.075s;\n  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  .hamburger--squeeze .hamburger-inner::before {\n    transition: top 0.075s 0.12s ease, opacity 0.075s ease; }\n  .hamburger--squeeze .hamburger-inner::after {\n    transition: bottom 0.075s 0.12s ease, transform 0.075s cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n\n.hamburger--squeeze.is-active .hamburger-inner {\n  transform: rotate(45deg);\n  transition-delay: 0.12s;\n  transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  .hamburger--squeeze.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0;\n    transition: top 0.075s ease, opacity 0.075s 0.12s ease; }\n  .hamburger--squeeze.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(-90deg);\n    transition: bottom 0.075s ease, transform 0.075s 0.12s cubic-bezier(0.215, 0.61, 0.355, 1); }\n\n/*\n   * Vortex\n   */\n.hamburger--vortex .hamburger-inner {\n  transition-duration: 0.2s;\n  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1); }\n  .hamburger--vortex .hamburger-inner::before, .hamburger--vortex .hamburger-inner::after {\n    transition-duration: 0s;\n    transition-delay: 0.1s;\n    transition-timing-function: linear; }\n  .hamburger--vortex .hamburger-inner::before {\n    transition-property: top, opacity; }\n  .hamburger--vortex .hamburger-inner::after {\n    transition-property: bottom, transform; }\n\n.hamburger--vortex.is-active .hamburger-inner {\n  transform: rotate(765deg);\n  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1); }\n  .hamburger--vortex.is-active .hamburger-inner::before, .hamburger--vortex.is-active .hamburger-inner::after {\n    transition-delay: 0s; }\n  .hamburger--vortex.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0; }\n  .hamburger--vortex.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(90deg); }\n\n/*\n   * Vortex Reverse\n   */\n.hamburger--vortex-r .hamburger-inner {\n  transition-duration: 0.2s;\n  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1); }\n  .hamburger--vortex-r .hamburger-inner::before, .hamburger--vortex-r .hamburger-inner::after {\n    transition-duration: 0s;\n    transition-delay: 0.1s;\n    transition-timing-function: linear; }\n  .hamburger--vortex-r .hamburger-inner::before {\n    transition-property: top, opacity; }\n  .hamburger--vortex-r .hamburger-inner::after {\n    transition-property: bottom, transform; }\n\n.hamburger--vortex-r.is-active .hamburger-inner {\n  transform: rotate(-765deg);\n  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1); }\n  .hamburger--vortex-r.is-active .hamburger-inner::before, .hamburger--vortex-r.is-active .hamburger-inner::after {\n    transition-delay: 0s; }\n  .hamburger--vortex-r.is-active .hamburger-inner::before {\n    top: 0;\n    opacity: 0; }\n  .hamburger--vortex-r.is-active .hamburger-inner::after {\n    bottom: 0;\n    transform: rotate(-90deg); }\n", ""]);

// exports


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(undefined);
// imports


// module
exports.push([module.i, ".menu-container {\n  width: 250px;\n  height: 100%;\n  position: fixed;\n  background: #fff;\n  top: 60px;\n  bottom: 0;\n  left: 0;\n  transition: all 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);\n  font-family: 'Arial',sans-serif;\n  overflow: auto;\n  height: calc(100% - 60px);\n  z-index: 9999999;\n  box-shadow: 0px 5px 2px #ccc; }\n\n.menu-container .show-menu {\n  left: 0; }\n\n.menu-container.hide-menu {\n  left: -250px; }\n\n.menu-open {\n  margin-left: 250px !important; }\n\n.cuppa-menu ul {\n  list-style: none;\n  padding: 0px; }\n\n.cuppa-menu ul > li {\n  cursor: pointer; }\n\n.sub-menu {\n  overflow: hidden; }\n\n.sub-menu > li > a {\n  padding-left: 50px !important; }\n\n.cuppa-menu ul > li > a {\n  display: block;\n  padding: 15px 30px;\n  background: #fff;\n  text-decoration: none;\n  border-bottom: 1px solid #ccc;\n  transition: all .3s linear; }\n\n.cuppa-menu ul > li > a:hover {\n  background: #007bff !important;\n  color: #fff; }\n\n.cuppa-menu ul > li > a.active {\n  background: #007bff !important;\n  color: #fff; }\n\n.cuppa-menu ul > li > a > i {\n  font-size: 24px;\n  float: right;\n  margin-top: -2px;\n  pointer-events: none; }\n\n.cuppa-menu-overlay {\n  position: fixed;\n  background: #000;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  display: none;\n  transition: all 1s linear; }\n\n.show-overlay {\n  display: block; }\n\n.hide-overlay {\n  display: none; }\n", ""]);

// exports


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<span (clickOutside)=\"closeMenu()\">\r\n    <button (click)=\"menuToggle()\" class=\"hamburger hamburger--{{config.animation}}\" [ngClass]=\"{'is-active': menuState, '': !menuState}\">\r\n  <span class=\"hamburger-box\">\r\n    <span class=\"hamburger-inner\"></span>\r\n</span>\r\n</button>\r\n<div class=\"menu-container cuppa-menu\" [ngClass]=\"{'show-menu': menuState, 'hide-menu': !menuState}\" [ngStyle]=\"{'top':config.offset.top+'px'}\">\r\n    <ul>\r\n        <li *ngFor=\"let item of menulist\">\r\n            <a *ngIf=\"item.subItems\" (click)=\"toggleSubMenu(item)\">{{item.title}} \r\n                <i *ngIf=\"item.subItems\" class=\"fa fa-angle-right\" [@toggleArrow]=\"item.expand == 'show' ? 'down': 'right'\"></i>\r\n            </a>\r\n            <a *ngIf=\"!item.subItems\" [ngClass]=\"{'active': item.active}\" (click)=\"onItemClick(item)\">{{item.title}} </a>\r\n            <ul *ngIf=\"item.subItems\" class=\"sub-menu\" [@toggleMenu]=\"item.expand == undefined ? 'hide': item.expand\">\r\n\r\n                <li *ngFor=\"let subitem of item.subItems\"><a (click)=\"onItemClick(subitem)\" [ngClass]=\"{'active': subitem.active}\">{{subitem.title}}</a></li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n</div>\r\n<span>\r\n<!--<div class=\"cuppa-menu-overlay\" [ngClass]=\"{'show-overlay': menuState, 'hide-overlay': !menuState}\"></div> -->"

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {


        var result = __webpack_require__(4);

        if (typeof result === "string") {
            module.exports = result;
        } else {
            module.exports = result.toString();
        }
    

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {


        var result = __webpack_require__(5);

        if (typeof result === "string") {
            module.exports = result;
        } else {
            module.exports = result.toString();
        }
    

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_10__;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_11__;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_12__;

/***/ }),
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var slideMenu_1 = __webpack_require__(0);
exports.SlideMenu = slideMenu_1.SlideMenu;
var clickOutside_1 = __webpack_require__(1);
exports.ClickOutsideDirective = clickOutside_1.ClickOutsideDirective;
var slideMenu_2 = __webpack_require__(0);
exports.SlideMenuModule = slideMenu_2.SlideMenuModule;


/***/ })
/******/ ]);
});
//# sourceMappingURL=index.umd.js.map