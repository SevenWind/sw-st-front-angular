import { EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
export declare class SlideMenu implements AfterViewInit {
    private _elementRef;
    private sanitizer;
    menulist: any;
    config: any;
    open: EventEmitter<number>;
    close: EventEmitter<number>;
    itemSelect: EventEmitter<number>;
    menuState: boolean;
    targetElement: any;
    overlayElem: any;
    defaultConfig: any;
    constructor(_elementRef: ElementRef, sanitizer: DomSanitizer);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    private menuToggle();
    private closeMenu();
    currentItem: any;
    private onItemClick(item);
    private toggleSubMenu(item);
    private addOverlayElement();
    private toggleOverlay();
}
export declare class SlideMenuModule {
}
