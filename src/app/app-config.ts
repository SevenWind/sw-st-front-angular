import { Injectable } from '@angular/core';
import * as _config from './app-config.json';

@Injectable()
export class Config {
  config = {
    AUTH_SERVICE_URL: '',
    RECAPTCHA_API_KEY: ''
  };

  constructor() {
    this.config = (<any>_config).default;
  }
}