import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from '../app-config';
import { Observable } from 'rxjs';
import { ServiceUtility } from './service-utility';
import { ToastrService } from 'ngx-toastr';
import ApiResponse from '../models/get-response.model';
import ResponseUser from '../models/user-response.model';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/internal/operators';
import BlackPassword, { BlackPasswordList } from '../models/black-password.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router,
    private config: Config, private toastr: ToastrService) { }

  readonly endpoint = this.config.config.AUTH_SERVICE_URL;

  /**
   * Login to system
   * @param loginForm user email and password
   */
  login(loginForm: { email: string; password: string }): Observable<boolean> {
    return this.http.post(this.endpoint + '/signin', loginForm, ServiceUtility.httpOptions).pipe(
      map((response: ApiResponse<any>) => {
        localStorage.setItem('is_admin', response.data[0].is_admin);
        localStorage.setItem('user_id', response.data[0].id);
        // localStorage.setItem('access_token', response.data[0].access_token);
        localStorage.setItem('user_name', response.data[0].name);
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('register', this.toastr, false)));
  }

  /**
   * Logout
   * @param userid 
   */
  logout(userid: string): Observable<boolean> {
    return this.http.post(this.endpoint + '/signout/' + userid, {}, ServiceUtility.httpOptions).pipe(
      map(response => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('register', this.toastr, false)));
  }

  /**
   * Restore address
   * @param addressForRestore phone or email for restore password
   */
  restorePassword(addressForRestore: string) {

  }

  /**
   * Create new user
   * @param user user data
   */
  register(user: User): Observable<boolean> {
    return this.http.post(this.endpoint + '/user', user, ServiceUtility.httpOptions).pipe(
      map((response: ApiResponse<any>) => true),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('register', this.toastr, false)));
  }

  /**
   * Get User by id
   * @param userid 
   */
  getUserInfo(userid: number): Observable<ResponseUser> {
    return this.http.get(this.endpoint + '/user/' + userid, ServiceUtility.httpOptions).pipe(
      map((response: ApiResponse<any>) => {
        let user = <ResponseUser>response.data[0];
        return new ResponseUser(user.id, user.name, user.email, user.phone, user.is_admin, user.is_blocked, user.is_accepted);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getUserInfo', this.toastr, null)));
  }

  /**
   * Update user info
   * @param user user data for update
   */
  updateUserInfo(user: User): Observable<boolean> {
    return this.http.put(this.endpoint + '/user/' + user.id, user, ServiceUtility.httpOptions).pipe(
      map(response => true),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('updateUserInfo', this.toastr, false)));
  }

  /**
   * Get list of all users
   */
  getUsers(): Observable<User[]> {
    return this.http.get(this.endpoint + '/users', ServiceUtility.httpOptions).pipe(
      map((response: ApiResponse<any>) => {
        return response.data.users.map((user: ResponseUser) => User.createWithEmptyPassword(user.id, user.name, user.email, user.phone, user.is_admin, user.is_blocked, user.is_accepted));
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getUsers', this.toastr, [])));
  }

  /**
   * Toggle isAdmin flag for user by his id
   * @param userid 
   */
  toggleAdminRole(userid: number, ): Observable<boolean> {
    return this.http.put(this.endpoint + '/toggleUserFlag/' + userid, { prop: "is_admin" }).pipe(
      map(response => true),
      catchError(ServiceUtility.handleErrorWithToast<any>('toggleAdminRole', this.toastr, false)));
  }

  /**
   * Toggle isBlockec flag for user by his id
   * @param userid 
   */
  toggleBlock(userid: number, ): Observable<boolean> {
    return this.http.put(this.endpoint + '/toggleUserFlag/' + userid, { prop: "is_blocked" }).pipe(
      map(response => true),
      catchError(ServiceUtility.handleErrorWithToast<any>('toggleBlock', this.toastr, false)));
  }

  /**
   * Toggle isAccepted flag for user by his id
   * @param userid 
   */
  toggleAccept(userid: number, ): Observable<boolean> {
    return this.http.put(this.endpoint + '/toggleUserFlag/' + userid, { prop: "is_accepted" }).pipe(
      map(response => true),
      catchError(ServiceUtility.handleErrorWithToast<any>('toggleAccept', this.toastr, false)));
  }

  /**
   * import black password list
   * @param passwordBlackList list of strings
   */
  importPasswordBlackList(passwordBlackList: string[]): Observable<number> {
    return this.http.post(this.endpoint + '/black_passwords', { passwords: passwordBlackList }, ServiceUtility.httpOptions).pipe(
      map((response: ApiResponse<any>) => response.data.count),
      catchError(ServiceUtility.handleErrorWithToast<any>('toggleAccept', this.toastr, 0)));
  }

  /**
   * Get black password list with sort, search and pagination
   * @param search search pwd string
   * @param sort name of column for sorting
   * @param sortDir asc or desc
   * @param pageSize page size. deafault is 10
   * @param pageIndex page infdex from 0 to...
   */
  getPasswordBlackList(search: string = '', sort: string = null, sortDir = null, pageSize = null, pageIndex = null): Observable<BlackPassword[]> {
    let params = new HttpParams()
      .set('search', search)
      .set('sort', (Object.is(sort, undefined) || sort == null ? '' : sort))
      .set('sortDir', (Object.is(sortDir, undefined) || sortDir == null ? '' : sortDir))
      .set('pageSize', (Object.is(pageSize, undefined) || pageSize == null ? '' : pageSize))
      .set('pageIndex', (Object.is(pageIndex, undefined) || pageIndex == null ? '' : pageIndex))
    let httpParams = ServiceUtility.httpOptions;
    httpParams.params = params;
    return this.http.get(this.endpoint + '/black_passwords', httpParams).pipe(
      map((response: ApiResponse<BlackPasswordList>) => {
        return response;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getUsers', this.toastr, [])));
  }

  /**
   * Remove password from black list
   * @param password 
   */
  removePasswordFromBlackList(password: string): Observable<boolean> {
    return this.http.post(this.endpoint + '/removePasswordFromBlackList', { password: password }, ServiceUtility.httpOptions).pipe(
      map(response => true),
      catchError(ServiceUtility.handleErrorWithToast<any>('toggleAccept', this.toastr, false)));
  }

  /**
   * Get access token
   */
  public getAccessToken(): string {
    return localStorage.getItem('access_token');
  }

  /**
    * User is logged??
    */
  public IsLoggedIn(): boolean {
    return localStorage.getItem('access_token') !== null && localStorage.getItem('access_token') !== '';
  }

  /**
   * Check if user is admin
   */
  public isAdmin(): string {
    //TODO: need String to Boolean convert
    return localStorage.getItem('is_admin');
  }

  /**
   * Get current user id
   */
  public getUserId(): string {
    return localStorage.getItem('user_id');
  }

  /**
   * Get current user name
   */
  public getUserName(): string {
    return localStorage.getItem('user_name');
  }

  public clearLocalStorage(): void {
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_name');
    localStorage.removeItem('is_admin');
    // localStorage.removeItem('access_token');
  }
}
