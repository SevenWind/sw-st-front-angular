import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';

export class ServiceUtility {

  public static httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    params: new HttpParams()
  };

  public static getHttpOptionsWithAuth(authService: AuthService) {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authService.getAccessToken()
      })
    };
  }

  public static extractRequestData(res: any) {
    const body = res;
    return body || {};
  }

  public static handleErrorWithToast<T>(operation = 'operation', toastr: any, result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        window.location.href = '/signin';
      }
      console.error(error);
      if (error.error != null && typeof error.error == 'object') {
        if ('message' in error.error) {
          if ('errors' in error.error) {
            toastr.error(error.error.errors[0].value, error.error.message);
          } else {
            toastr.error(error.error.message, 'Ошибка');
          }
        }
        toastr.error('Ошибка', 'Ошибка');
      } else {
        toastr.error(error.error, 'Ошибка');
      }
      return of(result as T);
    };
  }
}
