import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class SharedService {
    onTitleChangeEvent: EventEmitter<String> = new EventEmitter();
    loginTryCount = 0;
}