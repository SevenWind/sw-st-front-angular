export default class BlackPassword {
    password: string;
}

export interface BlackPasswordList {
    items: BlackPassword[];
    count: number;
}