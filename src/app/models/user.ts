export class User {
    id: number;
    name: string;
    email: string;
    phone: string;
    password: string;
    acceptPassword: string;
    isAdmin: boolean;
    isBlocked: boolean;
    isAccepted: boolean;

    public static createWithEmptyPassword(id: number, name: string, email: string,
        phone: string, isAdmin: boolean, isBlocked: boolean, isAccepted: boolean): User {
        var user = new User;
        user.id = id;
        user.name = name;
        user.email = email;
        user.phone = phone;
        user.isAdmin = isAdmin;
        user.isBlocked = isBlocked;
        user.isAccepted = isAccepted;
        user.password = "";
        user.acceptPassword = "";
        return user;
    }

    public static createWithPassword(id: number, name: string, email: string,
        phone: string, password: string, acceptPassword: string,
        isAdmin: boolean, isBlocked: boolean, isAccepted: boolean): User {
        var user = new User;
        user.id = id;
        user.name = name;
        user.email = email;
        user.phone = phone;
        user.isAdmin = isAdmin;
        user.isBlocked = isBlocked;
        user.isAccepted = isAccepted;
        user.password = "";
        user.acceptPassword = "";
        user.password = password;
        user.acceptPassword = acceptPassword;
        return user;
    }
}