export default class ResponseUser {
    id: number;
    name: string;
    email: string;
    phone: string;
    is_admin: boolean;
    is_blocked: boolean;
    is_accepted: boolean;

    constructor(id: number,
        name: string,
        email: string,
        phone: string,
        is_admin: boolean,
        is_blocked: boolean,
        is_accepted: boolean) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.is_admin = is_admin;
        this.is_blocked = is_blocked;
        this.is_accepted = is_accepted;
    }
}