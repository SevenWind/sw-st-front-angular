import { ValidatorFn, FormControl } from '@angular/forms';

export class EqualValidator {

    static mismatch(otherInputControl: FormControl): ValidatorFn {

        return (inputControl: FormControl): { [key: string]: boolean } | null => {
            if (inputControl.value !== undefined
                && inputControl.value.trim() != ""
                && inputControl.value !== otherInputControl.value) {
                return { 'mismatch': true };
            }

            return null;
        };
    }
}

export enum RegexPatterns {
    NAME_REGEX = '^[a-zA-Zа-яА-Я]{3,25}$',
    EMAIL_REGEX = '^([a-zA-Z0-9_.]+)+@([a-zA-Z0-9-_]+)+\.([a-zA-Z0-9]+)$',
    PHONE_REGEX = '^[0-9]{10,18}$',
    PASSWORD_REGEX = '^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,25}$'
}

export enum Errors {
    NOONE_BLACK_PASSWORD_ADDED_ERROR = 'В список не было добавлено ни одного пароля',
    SAVE_ERROR = 'Ошибка при сохранении',
    REGISTER_WITH_ERROR = 'Регистрация нового пользователя прошла с ошибкой',
    LOGOUT_ERROR = 'Произошла ошибка при попытке выйти из системы. Обратитетсь в поддержку.'
}

export enum Messages {
    SAVE_SUCCESS = 'Сохранение прошло успешно',
    REGISTER_SUCCESS = 'Регистрация нового пользователя прошла успешно',
}