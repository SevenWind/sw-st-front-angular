import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, of } from 'rxjs';
import BlackPassword from 'src/app/models/black-password.model';
import { AuthService } from 'src/app/services/auth.service';
import { catchError, finalize } from 'rxjs/operators';

export default class BlackPasswordListDataSource implements DataSource<BlackPassword> {

  private lessonsSubject = new BehaviorSubject<BlackPassword[]>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public itemsCount = 0;

  constructor(private authService: AuthService) { }

  connect(collectionViewer: CollectionViewer): Observable<BlackPassword[]> {
    return this.lessonsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.lessonsSubject.complete();
    this.loadingSubject.complete();
  }

  loadList(search = '', sortColName = null, sortDir = null, pageSize = 10, pageIndex = 0) {
    this.loadingSubject.next(true);
    this.authService.getPasswordBlackList(search, sortColName, sortDir, pageSize, pageIndex).pipe(
      catchError(() => of(null)),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe((list: any) => {
      this.itemsCount = list.count;
      this.lessonsSubject.next(list.items);
    });
  }
}