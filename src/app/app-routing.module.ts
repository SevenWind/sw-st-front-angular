import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './ui/auth/auth.component';
import { RegisterComponent } from './ui/register/register.component';
import { RestorePasswordComponent } from './ui/restore-password/restore-password.component';
import { UserListComponent } from './ui/admin/user-list/user-list.component';
import { BlackPasswordComponent } from './ui/admin/black-password/black-password.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AuthComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'restore-password', component: RestorePasswordComponent },
  { path: 'admin', redirectTo: 'admin/users', pathMatch: 'full' },
  { path: 'admin/users', component: UserListComponent },
  { path: 'admin/blackpasswords', component: BlackPasswordComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
