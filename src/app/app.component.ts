import { Component } from '@angular/core';
import { SharedService } from './services/shared-variables-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Система управления ставками онлайн';

  constructor(private sharedService: SharedService) {
    sharedService.onTitleChangeEvent.subscribe(
      (newTitle) => {
        this.title = newTitle;
      }
    );
  }
}
