import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared-variables-service';
import { Validators, FormControl } from '@angular/forms';
import { RegexPatterns } from 'src/app/utility/consts';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss']
})


export class RestorePasswordComponent implements OnInit {
  private email_placeholder = "Введите почту";
  private phone_placeholder = "Введите телефон";
  private inputFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX)]);

  title = "Восстановление пароля"
  input_placeholder = this.email_placeholder;

  constructor(private router: Router, private sharedService: SharedService) {
    this.sharedService.onTitleChangeEvent.emit(this.title);
  }

  ngOnInit() {
    document.body.className = "auth-body";
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  toLoginPage() {
    this.router.navigateByUrl('/login');
  }

  restorePassword() {
    if (!this.inputFormControl.valid) {
      return;
    }
    this.router.navigateByUrl('/restore-password');
  }

  toggleInputType() {
    if (this.input_placeholder == this.email_placeholder) {
      this.input_placeholder = this.phone_placeholder;
      this.inputFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.PHONE_REGEX)]);
    } else {
      this.input_placeholder = this.email_placeholder;
      this.inputFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX)]);
    }
  }
}
