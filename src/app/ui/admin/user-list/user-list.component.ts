import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { User } from '../../../models/user'
import { AuthService } from 'src/app/services/auth.service';
import { UserEditDialogComponent } from '../../dialogs/user-edit-dialog/user-edit-dialog.component';
import ResponseUser from 'src/app/models/user-response.model';
import { ToastrService } from 'ngx-toastr';
import { UserCreateDialogComponent } from '../../dialogs/user-create-dialog/user-create-dialog.component';
import { Errors, Messages } from 'src/app/utility/consts';

const ELEMENT_DATA: User[] = [

];

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  private displayedColumns: string[] = ['id', 'name', 'email', 'phone', 'isAdmin', 'isBlocked', 'isAccepted', 'actions'];
  private dataSource = new MatTableDataSource(ELEMENT_DATA);
  private userEditDialogRef = null;
  private title = "Реестр пользователей"

  constructor(private router: Router, private authService: AuthService,
    public dialog: MatDialog, private toastr: ToastrService) { }

  ngOnInit() {
    document.body.className = "admin-body";
    this.updateUserList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  openPath(clickedMenuItem: any) {
    this.router.navigateByUrl(clickedMenuItem.link);
  }

  toggleAdmin(userid: number) {
    this.authService.toggleAdminRole(userid).subscribe(result => {
      if (result) {
        this.toastr.success('', Messages.SAVE_SUCCESS);
        this.updateUserList();
      } else {
        this.toastr.error('', Errors.SAVE_ERROR);
      }
    });
  }

  toggleBlock(userid: number) {
    this.authService.toggleBlock(userid).subscribe(result => {
      if (result) {
        this.toastr.success('', Messages.SAVE_SUCCESS);
        this.updateUserList();
      } else {
        this.toastr.error('', Errors.SAVE_ERROR);
      }
    });
  }

  toggleAccept(userid: number) {
    this.authService.toggleAccept(userid).subscribe(result => {
      if (result) {
        this.toastr.success('', Messages.SAVE_SUCCESS);
        this.updateUserList();
      } else {
        this.toastr.error('', Errors.SAVE_ERROR);
      }
    });
  }

  updateUserList() {
    this.authService.getUsers().subscribe(users => {
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  logout() {
    this.authService.logout(localStorage.getItem('user_id')).subscribe(res => {
      if (res) {
        this.router.navigateByUrl('');
      } else {
        this.toastr.error(Errors.LOGOUT_ERROR);
      }
    });
  }

  openEditForm(userid: number) {
    if (userid) {
      this.authService.getUserInfo(userid).subscribe((userInfo: ResponseUser) => {
        this.userEditDialogRef = this.dialog.open(UserEditDialogComponent, {
          minWidth: '600px',
          maxWidth: '80%',
          data: userInfo
        });
        this.userEditDialogRef.afterClosed().subscribe((result: User) => {
          if (result != null) {
            this.authService.updateUserInfo(result).subscribe(result => {
              if (result) {
                this.toastr.success('', Messages.SAVE_SUCCESS);
                this.updateUserList();
              } else {
                this.toastr.error('', Errors.SAVE_ERROR);
              }
            });
          }
        });
      });
    } else {
      this.userEditDialogRef = this.dialog.open(UserCreateDialogComponent, {
        minWidth: '600px',
        maxWidth: '80%',
        data: new User()
      });
      this.userEditDialogRef.afterClosed().subscribe((result: User) => {
        if (result != null) {
          this.authService.register(result).subscribe(result => {
            if (result) {
              this.toastr.success('', Messages.REGISTER_SUCCESS);
              this.updateUserList();
            } else {
              this.toastr.error('', Errors.REGISTER_WITH_ERROR);
            }
          });
        }
      });
    }
  }

  private menuItemsArray: any[] = [
    { "title": "На главную страницу", "link": "/" },
    {
      "title": "Таблицы", "link": "#",
      "subItems": [
        { "title": "Пользователи", "link": "admin/users" },
        { "title": "Недопустимые пароли", "link": "admin/blackpasswords" },
      ]
    },
  ];
}