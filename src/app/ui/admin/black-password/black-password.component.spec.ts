import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackPasswordComponent } from './black-password.component';

describe('BlackPasswordComponent', () => {
  let component: BlackPasswordComponent;
  let fixture: ComponentFixture<BlackPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
