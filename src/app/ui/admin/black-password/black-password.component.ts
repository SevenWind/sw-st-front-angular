import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AddBlackPasswordSingleComponent } from '../../dialogs/add-black-password-single/add-black-password-single.component';
import { declOfNum } from 'src/app/utility/text-utility';
import { ImportPassBlackListComponent } from '../../dialogs/import-pass-black-list/import-pass-black-list.component';
import { merge, of as observableOf, fromEvent } from 'rxjs';
import { catchError, map, startWith, switchMap, tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import BlackPasswordListDataSource from 'src/app/utility/data-sources/black-pwd-list-data-source';
import { Errors } from 'src/app/utility/consts';

@Component({
  selector: 'app-black-password',
  templateUrl: './black-password.component.html',
  styleUrls: ['./black-password.component.scss']
})
export class BlackPasswordComponent implements OnInit {
  private title = "Реестр недопустимых паролей"
  private displayedColumns: string[] = ['password', 'actions'];
  private dialogRef = null;
  private dataSource: BlackPasswordListDataSource;
  @ViewChild(MatPaginator, { static: true }) private paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) private sort: MatSort;
  @ViewChild('search', { static: true }) search: ElementRef;

  constructor(private router: Router, public dialog: MatDialog,
    private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit() {
    document.body.className = "admin-body";
    this.dataSource = new BlackPasswordListDataSource(this.authService);
    this.loadBlackPwds();
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  ngAfterViewInit() {
    fromEvent(this.search.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadBlackPwds();
        })
      )
      .subscribe();

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadBlackPwds())
      ).subscribe();
  }

  logout() {
    this.authService.logout(this.authService.getUserId()).subscribe(res => {
      if (res) {
        this.authService.clearLocalStorage();
        this.router.navigateByUrl('');
      } else {
        this.toastr.error(Errors.LOGOUT_ERROR);
      }
    });
  }

  loadBlackPwds(): void {
    this.dataSource.loadList(this.search.nativeElement.value, this.sort.active, this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
  }

  openPath(clickedMenuItem: any) {
    this.router.navigateByUrl(clickedMenuItem.link);
  }

  removePassword(password: string) {
    this.authService.removePasswordFromBlackList(password).subscribe(res => {
      this.loadBlackPwds();
    });
  }

  openAddForm() {
    this.dialogRef = this.dialog.open(AddBlackPasswordSingleComponent, {
      minWidth: '600px',
      maxWidth: '80%',
      data: {}
    });
    this.dialogRef.afterClosed().subscribe((result: string) => {
      if (result != null && result != "") {
        this.authService.importPasswordBlackList([result,]).subscribe(result => {
          if (result) {
            this.toastr.success('', 'В список было добавлено ' + result + ' ' + declOfNum(result, ['пароль', 'пароля', 'паролей']));
            this.loadBlackPwds();
          } else {
            this.toastr.error('', Errors.NOONE_BLACK_PASSWORD_ADDED_ERROR);
          }
        });
      }
    });
  }

  openImportForm() {
    this.dialogRef = this.dialog.open(ImportPassBlackListComponent, {
      minWidth: '600px',
      maxWidth: '80%',
      data: {}
    });
    this.dialogRef.afterClosed().subscribe((result: File | any) => {
      let myReader = new FileReader();
      let $this = this;
      myReader.addEventListener("loadend", function (e: any) {
        $this.authService.importPasswordBlackList(e.srcElement.result.split('\n')).subscribe(result => {
          if (result) {
            $this.toastr.success('', 'В список было добавлено ' + result + ' ' + declOfNum(result, ['пароль', 'пароля', 'паролей']));
            $this.loadBlackPwds();
          } else {
            $this.toastr.error('', Errors.NOONE_BLACK_PASSWORD_ADDED_ERROR);
          }
        });
      });
      myReader.readAsText(result.file);
    });
  }

  private menuItemsArray: any[] = [
    { "title": "На главную страницу", "link": "/" },
    {
      "title": "Таблицы", "link": "#",
      "subItems": [
        { "title": "Пользователи", "link": "admin/users" },
        { "title": "Недопустимые пароли", "link": "admin/blackpasswords" },
      ]
    },
  ];
}
