import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth/auth.component';
import { RegisterComponent } from './register/register.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { MaterialSetModule } from '../material-set.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent } from './admin/user-list/user-list.component';
import { SlideMenuModule } from '../../../src/assets/modules/cuppa-ng2-slidemenu/cuppa-ng2-slidemenu';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { BlackPasswordComponent } from './admin/black-password/black-password.component';
import { ToastrModule } from 'ngx-toastr';
import { UserEditDialogComponent } from './dialogs/user-edit-dialog/user-edit-dialog.component';
import { UserCreateDialogComponent } from './dialogs/user-create-dialog/user-create-dialog.component';
import { AddBlackPasswordSingleComponent } from './dialogs/add-black-password-single/add-black-password-single.component';
import { ImportPassBlackListComponent } from './dialogs/import-pass-black-list/import-pass-black-list.component';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

const config: InputFileConfig = {
  fileAccept: ".txt"
};

@NgModule({
  declarations: [
    AuthComponent,
    RegisterComponent,
    RestorePasswordComponent,
    UserListComponent,
    BlackPasswordComponent,
    UserEditDialogComponent,
    UserCreateDialogComponent,
    AddBlackPasswordSingleComponent,
    ImportPassBlackListComponent
  ],
  entryComponents: [
    UserEditDialogComponent,
    UserCreateDialogComponent,
    AddBlackPasswordSingleComponent,
    ImportPassBlackListComponent
  ],
  imports: [
    MaterialSetModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SlideMenuModule,
    InputFileModule.forRoot(config),
    RecaptchaModule.forRoot({
      siteKey: "6LdEWKsUAAAAANF_o3q7g_z__PpyyFIQFOEtjSrb",
    }),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      maxOpened: 10,
    })
  ]
})
export class UiModule { }
