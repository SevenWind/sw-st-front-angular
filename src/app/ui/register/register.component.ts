import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared-variables-service';
import { Validators, FormControl } from '@angular/forms';
import { EqualValidator, Errors, Messages, RegexPatterns } from 'src/app/utility/consts';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private nameFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.NAME_REGEX)]);
  private emailFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX), Validators.maxLength(30)]);
  private phoneFormControl = new FormControl('', [Validators.pattern(RegexPatterns.PHONE_REGEX)]);
  private passwordFormControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);
  private acceptPasswordFormControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), EqualValidator.mismatch(this.passwordFormControl), Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);
  private myRecaptcha = new FormControl(false);
  title = "Регистрация"

  constructor(private router: Router, private sharedService: SharedService,
    private authService: AuthService, private toastr: ToastrService) {
    this.sharedService.onTitleChangeEvent.emit(this.title);
  }

  ngOnInit() {
    document.body.className = "auth-body";
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  toLoginPage() {
    this.router.navigateByUrl('/login');
  }

  register() {
    if (this.myRecaptcha.valid
      && this.nameFormControl.valid
      && this.emailFormControl.valid
      && this.phoneFormControl.valid
      && this.passwordFormControl.valid
      && this.acceptPasswordFormControl.valid) {
      const user = User.createWithPassword(
        0, this.nameFormControl.value, this.emailFormControl.value,
        this.phoneFormControl.value, this.passwordFormControl.value,
        this.acceptPasswordFormControl.value, false, false, false
      );
      this.authService.register(user).subscribe(result => {
        if (result) {
          this.toastr.success('', Messages.REGISTER_SUCCESS);
          this.router.navigateByUrl('/admin');
        } else {
          this.toastr.error('', Errors.REGISTER_WITH_ERROR);
        }
      },
        () => { });
    }
  }

  onScriptLoad() {
    // console.log('Google reCAPTCHA loaded and is ready for use!')
  }

  onScriptError() {
    // console.log('Something went long when loading the Google reCAPTCHA')
  }
}
