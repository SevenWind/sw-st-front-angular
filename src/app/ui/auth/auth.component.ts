import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared-variables-service';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { RegexPatterns } from 'src/app/utility/consts';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  private emailFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX)]);
  // private passwordFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);
  private passwordFormControl = new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]){8,25}/)]);
  private myRecaptcha = new FormControl(false);
  title = 'Авторизация';
  showCaptcha = false;
  isSaveEnabled: boolean;

  constructor(private router: Router,
    private authService: AuthService,
    private sharedService: SharedService) {
    this.sharedService.onTitleChangeEvent.emit(this.title);
  }

  ngOnInit() {
    document.body.className = "auth-body";
    this.passwordFormControl.valueChanges.subscribe(res => {
      if (this.passwordFormControl.valid) {
        this.isSaveEnabled = true;
      } else {
        this.isSaveEnabled = false;
      }
    });
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  auth() {
    if (!this.passwordFormControl.valid || !this.emailFormControl.valid) {
      return;
    }
    if (this.showCaptcha) {
      if (!this.myRecaptcha.valid) {
        return;
      }
    }
    this.authService.login(
      {
        email: this.emailFormControl.value,
        password: this.passwordFormControl.value
      }
    ).subscribe((authIsSuccess) => {
      if (authIsSuccess) {
        this.router.navigateByUrl('/admin');
      } else {
        this.sharedService.loginTryCount++;
        console.warn("Перенести на сервер или сервис количество попыток авторизоваться ?");
        if (this.sharedService.loginTryCount > 4) {
          this.showCaptcha = true;
        }
      }
    });
  }

  toRegisterPage() {
    this.router.navigateByUrl('/register');
  }

  toRestorePassword() {
    this.router.navigateByUrl('/restore-password');
  }
}
