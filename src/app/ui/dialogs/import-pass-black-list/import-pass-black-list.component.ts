import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-import-pass-black-list',
  templateUrl: './import-pass-black-list.component.html',
  styleUrls: ['./import-pass-black-list.component.scss']
})
export class ImportPassBlackListComponent implements OnInit {
  private configFile = new FormControl(null, [Validators.required]);
  public isSaveEnabled = false;
  constructor(public dialogRef: MatDialogRef<ImportPassBlackListComponent>) { }

  ngOnInit() {
    this.configFile.valueChanges.subscribe(res => {
      if (this.configFile.valid) {
        this.isSaveEnabled = true;
      } else {
        this.isSaveEnabled = false;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  onSaveClick(): void {
    if (this.configFile.valid) {
      this.dialogRef.close(this.configFile.value[0]);
    }
  }
}

