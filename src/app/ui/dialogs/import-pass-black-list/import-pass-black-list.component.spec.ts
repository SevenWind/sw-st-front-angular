import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportPassBlackListComponent } from './import-pass-black-list.component';

describe('ImportPassBlackListComponent', () => {
  let component: ImportPassBlackListComponent;
  let fixture: ComponentFixture<ImportPassBlackListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportPassBlackListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportPassBlackListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
