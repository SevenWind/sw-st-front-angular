import { Component, Inject } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { RegexPatterns } from 'src/app/utility/consts';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-create-dialog',
  templateUrl: './user-create-dialog.component.html',
  styleUrls: ['./user-create-dialog.component.scss']
})
export class UserCreateDialogComponent {
  public nameFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.NAME_REGEX)]);
  public emailFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX), Validators.maxLength(40)]);
  public phoneFormControl = new FormControl('', [Validators.pattern(RegexPatterns.PHONE_REGEX)]);
  private passwordFormControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);
  private acceptPasswordFormControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);

  constructor(
    public dialogRef: MatDialogRef<UserCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User) { }

  onNoClick(): void {
    this.dialogRef.close(null);
  }
  onSaveClick(): void {
    if (this.nameFormControl.valid
      && this.emailFormControl.valid
      && this.phoneFormControl.valid
      && this.passwordFormControl.valid
      && this.acceptPasswordFormControl.valid) {
      this.dialogRef.close(
        User.createWithPassword(0, this.nameFormControl.value,
          this.emailFormControl.value,
          this.phoneFormControl.value,
          this.passwordFormControl.value,
          this.acceptPasswordFormControl.value, false, false, false)
      );
    }
  }

}
