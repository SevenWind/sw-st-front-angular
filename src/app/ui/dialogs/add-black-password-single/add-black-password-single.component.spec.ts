import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBlackPasswordSingleComponent } from './add-black-password-single.component';

describe('AddBlackPasswordSingleComponent', () => {
  let component: AddBlackPasswordSingleComponent;
  let fixture: ComponentFixture<AddBlackPasswordSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBlackPasswordSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBlackPasswordSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
