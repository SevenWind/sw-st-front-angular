import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { RegexPatterns } from 'src/app/utility/consts';

@Component({
  selector: 'app-add-black-password-single',
  templateUrl: './add-black-password-single.component.html',
  styleUrls: ['./add-black-password-single.component.scss']
})
export class AddBlackPasswordSingleComponent implements OnInit {
  public passwordFormControl = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(25), Validators.pattern(RegexPatterns.PASSWORD_REGEX)]);
  public isSaveEnabled = false;

  constructor(
    public dialogRef: MatDialogRef<AddBlackPasswordSingleComponent>) { }

  ngOnInit(): void {
    this.passwordFormControl.valueChanges.subscribe(res => {
      if (this.passwordFormControl.valid) {
        this.isSaveEnabled = true;
      } else {
        this.isSaveEnabled = false;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  onSaveClick(): void {
    if (this.passwordFormControl.valid) {
      this.dialogRef.close(this.passwordFormControl.value);
    }
  }
}
