import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import ResponseUser from 'src/app/models/user-response.model';
import { Validators, FormControl } from '@angular/forms';
import { RegexPatterns } from 'src/app/utility/consts';

@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: './user-edit-dialog.component.html',
  styleUrls: ['./user-edit-dialog.component.scss']
})
export class UserEditDialogComponent {
  public nameFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.NAME_REGEX)]);
  public emailFormControl = new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.EMAIL_REGEX), Validators.maxLength(40)]);
  public phoneFormControl = new FormControl('', [Validators.pattern(RegexPatterns.PHONE_REGEX)]);
  public isAdminFormControl = new FormControl();
  public isBlockedFormControl = new FormControl();
  public isAcceptedFormControl = new FormControl();

  constructor(
    public dialogRef: MatDialogRef<UserEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ResponseUser) {
    if (data != null) {
      this.nameFormControl.setValue(data.name);
      this.emailFormControl.setValue(data.email);
      this.phoneFormControl.setValue(data.phone);
      this.isAdminFormControl.setValue(data.is_admin);
      this.isBlockedFormControl.setValue(data.is_blocked);
      this.isAcceptedFormControl.setValue(data.is_accepted);
    }
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }
  onSaveClick(): void {
    if (this.nameFormControl.valid
      && this.emailFormControl.valid
      && this.phoneFormControl.valid) {
      this.dialogRef.close(
        new ResponseUser(this.data.id, this.nameFormControl.value,
          this.emailFormControl.value,
          this.phoneFormControl.value,
          this.isAdminFormControl.value,
          this.isBlockedFormControl.value,
          this.isAcceptedFormControl.value)
      );
    }
  }
}
