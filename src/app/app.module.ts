import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { SharedService } from './services/shared-variables-service';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Config } from './app-config';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    UiModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  exports: [
    RecaptchaModule
  ],
  providers: [
    SharedService,
    HttpClient,
    Config
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }