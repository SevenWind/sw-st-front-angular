import { MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatSlideToggleModule, MatMenuModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule, MatExpansionModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatSpinner, MatProgressSpinnerModule } from '@angular/material'
import { NgModule } from '@angular/core';

const materialModules = [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatToolbarModule,
    MatExpansionModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatProgressSpinnerModule
];

@NgModule({
    imports: [...materialModules],
    exports: [...materialModules],
    declarations: []
})
export class MaterialSetModule {
}